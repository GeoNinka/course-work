from django.core.management.base import BaseCommand
from product.models import Product

class Command(BaseCommand):
    help = "get average orice"

    def handle(self, *args, **kwargs):
        counter = 0
        summ = 0
        products = Product.objects.all()
        for product in products:
            summ = summ + product.price
            counter = counter + 1
        self.stdout.write("Средняя стоимость услуг : " + str(summ/counter))