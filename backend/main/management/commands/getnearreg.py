from django.core.management.base import BaseCommand
from reg.models import Reg
import datetime

class Command(BaseCommand):
    help = "get nearest registration"

    def handle(self, *args, **kwargs):
        registrations = Reg.objects.all()
        serviceTime = str(registrations[0].time)
        serviceName = ''
        for registration in registrations:
            if str(registration.time) > serviceTime:
                serviceTime = str(registration.time)[:-6]
                serviceName = registration.service.all()[0].name
        self.stdout.write(serviceName + " " + serviceTime)
