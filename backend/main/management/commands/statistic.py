from django.core.management.base import BaseCommand
from reg.models import Reg
from authentication.models import User
from service.models import Service
from master.models import Master
import datetime

class Command(BaseCommand):
    help = "get statictic"

    def handle(self, *args, **kwargs):
        self.stdout.write("Пользователей: " + str(len(User.objects.all())))
        self.stdout.write("Мастеров: " + str(len(Master.objects.all())))
        self.stdout.write("Услуг: " + str(len(Service.objects.all())))