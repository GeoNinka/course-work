from django.shortcuts import render
from shedule.models import Shedule
from product.models import Product
from service.models import Service
from master.models import Master

from reg.models import Reg
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination 
import xlwt
from django.http import HttpResponse

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string 

def sendMail():
    html_body = render_to_string('main/email.html')
    msg = EmailMultiAlternatives(subject="Тема", to=["vpometun2001@yandex.ru"])
    msg.attach_alternative(html_body, 'text/html')
    msg.send()

def index(request):
    return render(request, 'main/index.html')

class ServiceApiListPagination(PageNumberPagination):
    page_size = 2
    page_size_query_param = 'page_size'
    max_page_size = 100

def service(request):
    pagination_class = ServiceApiListPagination

    services = Service.objects.all()
    masters = Master.objects.all()
    arr = services
    if request.GET.getlist('master'):
        if request.GET.getlist('master')[0]:
            arr = []
            for service in services:
                if int(request.GET.getlist('master')[0]) == int(service.master.all()[0].id):
                    arr.append(service)
    # arr = Service.objects.filter(Q(master.all()[0].id = int(request.GET.getlist('master')[0])))
    # arr = Service.objects.filter(Q(name = "Модельная стрижка")
    return render(request, 'main/service.html', {'services': services, 'masters': masters, 'arr': arr})

def product(request):
    products = Product.objects.all()
    return render(request, 'main/products.html', {'products': products})

def master(request):
    masters = Master.objects.all()
    products = Product.objects.all()
    return render(request, 'main/masters.html', {'masters': masters, 'products': products})

def export_excel(request):
    reg = Reg.objects.all()


    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment;filename="reg.xls"'

    wb = xlwt.Workbook(encoding="utf-8")
    ws = wb.add_sheet('Registrations')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = ['Client', 'Date', 'Master', 'Phone number']
    
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()

    for elem in reg:
        row_num = row_num + 1
        ws.write(row_num, 0, str(elem.user.all()[0].first_name))
        ws.write(row_num, 1, str(elem.time))
        ws.write(row_num, 2, str(elem.master.all()[0]))
        ws.write(row_num, 3, str(elem.user.all()[0].phone))
    wb.save(response)
    return response