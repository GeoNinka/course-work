from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('service', views.service, name='service'),
    path('masters', views.master, name='masters'),
    path('products', views.product, name='products'),
    path('export_excel', views.export_excel, name='export_excel'),
]
