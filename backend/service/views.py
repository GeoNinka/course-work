from service.serializer import ServiceSerializer
from service.models import Service
from rest_framework.viewsets import ModelViewSet
from rest_framework.filters import SearchFilter
from django_filters import rest_framework

class ServiceViewSet(ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    filter_backends = (rest_framework.DjangoFilterBackend, SearchFilter)
    filterset_fields = ('id', 'name')
    search_fields = ('id', 'name')