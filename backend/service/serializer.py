from rest_framework import serializers
from service.models import Service
from master.models import Master

class MasterSerializerForService(serializers.ModelSerializer):
    class Meta:
        model = Master
        fields = '__all__'

class ServiceSerializer(serializers.ModelSerializer):
    master_data = MasterSerializerForService(source='master', many=True)
    class Meta:
        model = Service
        # exclude = ['master']
        fields = '__all__'