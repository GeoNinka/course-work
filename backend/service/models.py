from django.db import models
from master.models import Master

class Service(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание услуги')
    price = models.FloatField(verbose_name='Цена')
    photo = models.ImageField(verbose_name='Изображение', upload_to='media/services')
    master = models.ManyToManyField(to=Master)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Услуга'
        verbose_name_plural = 'Услуги'

# Create your models here.
