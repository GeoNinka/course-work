from reg.serializer import RegSerializer
from reg.models import Reg
from rest_framework.viewsets import ModelViewSet

class RegViewSet(ModelViewSet):
    queryset = Reg.objects.all()
    serializer_class = RegSerializer