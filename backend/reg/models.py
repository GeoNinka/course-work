from django.db import models
from master.models import Master
from service.models import Service
from authentication.models import User

class Reg(models.Model):
    service = models.ManyToManyField(to=Service, related_name='regis')
    master = models.ManyToManyField(to=Master)
    user = models.ManyToManyField(to=User)
    time = models.DateTimeField(verbose_name='Время приёма')

    def __str__(self):
        return 'Запись'
    
    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'

# Create your models here.
