from dataclasses import field
from rest_framework import serializers
from reg.models import Reg
from master.models import Master
from service.models import Service
from authentication.models import User

class UserSerializerForReg(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class ServiceSerializerForReg(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = '__all__'

class MasterSerializerForReg(serializers.ModelSerializer):
    class Meta:
        model = Master
        fields = '__all__'

class RegSerializer(serializers.ModelSerializer):
    user_data = UserSerializerForReg(source='user', many=True)
    service_data = ServiceSerializerForReg(source='service', many=True)
    master_data = MasterSerializerForReg(source='master', many=True)

    class Meta:
        model = Reg
        exclude = ['master', 'user', 'service']