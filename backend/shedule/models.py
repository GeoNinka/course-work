from django.db import models

class Shedule(models.Model):
    monday = models.CharField(verbose_name='Понедельник', max_length=255)
    tuesday = models.CharField(verbose_name='Вторник', max_length=255)
    wednesday = models.CharField(verbose_name='Среда', max_length=255)
    thursday = models.CharField(verbose_name='Четверг', max_length=255)
    friday = models.CharField(verbose_name='Пятница', max_length=255)
    saturday = models.CharField(verbose_name='Суббота', max_length=255)
    sunday = models.CharField(verbose_name='Понедельник', max_length=255)
    

    def __str__(self):
        return self.monday
    
    class Meta:
        verbose_name = 'График'
        verbose_name_plural = 'Графики'
