from shedule.serializer import SheduleSerializer
from shedule.models import Shedule
from rest_framework.viewsets import ModelViewSet

class SheduleViewSet(ModelViewSet):
    queryset = Shedule.objects.all()
    serializer_class = SheduleSerializer