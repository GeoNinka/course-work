from django.contrib import admin
from shedule.models import Shedule


class SheduleAdmin(admin.ModelAdmin):
    list_display = ('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday')

admin.site.register(Shedule, SheduleAdmin)
# Register your models here.
