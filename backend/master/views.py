from master.serializer import MasterSerializer
from master.models import Master
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import render 

class MasterViewSet(ModelViewSet):
    queryset = Master.objects.all()
    serializer_class = MasterSerializer
