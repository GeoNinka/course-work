from django.db import models
from shedule.models import Shedule

class Master(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Услуги')
    photo = models.ImageField(verbose_name='Изображение', upload_to='media/masters')
    shedule = models.ManyToManyField(to=Shedule)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Мастер'
        verbose_name_plural = 'Мастера'

# Create your models here.
