from rest_framework import serializers
from master.models import Master
from shedule.models import Shedule

class SheduleSerializerForMaster(serializers.ModelSerializer):
    class Meta:
        model = Shedule
        fields = '__all__'

class MasterSerializer(serializers.ModelSerializer):
    shedule_data = SheduleSerializerForMaster(source='shedule', many=True)
    class Meta:
        model = Master
        exclude = ['shedule']

