# Generated by Django 4.0.5 on 2022-07-10 11:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shedule', '0001_initial'),
        ('master', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='master',
            name='shedule',
            field=models.ManyToManyField(to='shedule.shedule'),
        ),
    ]
