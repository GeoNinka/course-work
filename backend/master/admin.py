from django.contrib import admin
from master.models import Master


class MasterAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')
    # list_filter = ('description')

admin.site.register(Master, MasterAdmin)

# Register your models here.
