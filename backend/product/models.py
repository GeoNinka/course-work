from email.mime import image
from tabnanny import verbose
from django.db import models

class Product(models.Model):
    name = models.CharField(verbose_name='Название', max_length=255)
    description = models.TextField(verbose_name='Описание')
    photo = models.ImageField(verbose_name='Изображение', upload_to='media/products')
    price = models.FloatField(verbose_name='Стоимость')

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Товар'
        verbose_name_plural = 'Товары'
