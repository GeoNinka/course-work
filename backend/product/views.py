from product.serializer import ProductSerializer
from product.models import Product
from rest_framework.viewsets import ModelViewSet    
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.decorators import action


class ProductViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.ListModelMixin,
                    viewsets.GenericViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_serializer_class(self):
        if self.action == "product_price":
            return PostAuthorSerializer
        else:
            return self.serializer_class

    @action(detail=True, methods=["get"], url_path=r'product-price',)
    def product_price(self, request, pk=None):
        post = self.get_object()
        serializer = self.get_serializer(product)
        return Response(serializer.data, status=status.HTTP_200_OK)
