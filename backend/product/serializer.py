from rest_framework import serializers
from product.models import Product
from rest_framework.serializers import ValidationError


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
        validators = [

        ]
    
    def validate_price(self, price):
        if price < 0:
            raise ValidationError("Цена должна быть больше нуля!")
        return price
