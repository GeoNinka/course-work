from rest_framework.routers import DefaultRouter
from shedule.views import SheduleViewSet
from service.views import ServiceViewSet
from reg.views import RegViewSet
from product.views import ProductViewSet
from master.views import MasterViewSet
from authentication.views import UserViewSet


router = DefaultRouter()

router.register('shedule', SheduleViewSet)
router.register('service', ServiceViewSet)
router.register('reg', RegViewSet)
router.register('product', ProductViewSet)
router.register('master', MasterViewSet)
router.register('authentication', UserViewSet)
router.register(r'product', ProductViewSet)